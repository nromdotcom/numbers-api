streamarn=`docker run -e AWS_DEFAULT_REGION=fake -e AWS_ACCESS_KEY_ID=fake -e AWS_SECRET_ACCESS_KEY=fake --rm --network lambda_local \
  lambci/lambda:build-nodejs8.10 aws dynamodbstreams list-streams --endpoint http://dynamodb:8000 --output text | cut -f2`

shardid=`docker run -e AWS_DEFAULT_REGION=fake -e AWS_ACCESS_KEY_ID=fake -e AWS_SECRET_ACCESS_KEY=fake --rm --network lambda_local \
  lambci/lambda:build-nodejs8.10 aws dynamodbstreams describe-stream --endpoint http://dynamodb:8000 --output text \
  --stream-arn $streamarn --query StreamDescription.Shards[0].ShardId`

iterator=`docker run -e AWS_DEFAULT_REGION=fake -e AWS_ACCESS_KEY_ID=fake -e AWS_SECRET_ACCESS_KEY=fake --rm --network lambda_local \
  lambci/lambda:build-nodejs8.10 aws dynamodbstreams get-shard-iterator --endpoint http://dynamodb:8000 --output text \
  --stream-arn $streamarn --shard-id $shardid --shard-iterator-type TRIM_HORIZON`

records=`docker run -e AWS_DEFAULT_REGION=fake -e AWS_ACCESS_KEY_ID=fake -e AWS_SECRET_ACCESS_KEY=fake --rm --network lambda_local \
  lambci/lambda:build-nodejs8.10 aws dynamodbstreams get-records --shard-iterator $iterator --endpoint http://dynamodb:8000`

echo $records | sam local invoke ProcessNumberActions --docker-network lambda_local