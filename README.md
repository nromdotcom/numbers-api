# Number Mutation API

This is an example of an event-sourced-approaching-CQRS Serverless application.
It is meant to demonstrate the power, flexibility, and extendibility of the
serverless paradigm for designing APIs.

Specifically, this application is an API that can be used for creating and
updating numbers using certain CQRS principles.

Basically, the exposed API allows you to create numbers and change them via simple arithmetic.
Each arithmetic operation is stored in a DynamoDB table and gets processed via DynamoDB Streams
in order to update the number.

## API

* `GET /:numberId` - Fetch the current value of a number tracked by the application
  * Returns: The current value of the number with the requested id or `0` if it does not exist.
  * Example: `GET /123` will return the current value of a number with the id of `123` (or `0`
  it does not exist).
* `PUT /:numberId/mutate/:action/:amount` - Apply an arithmetic operation to a specific number
  * Returns: An `operationId` to track the status of the requested mutation operation.
  * Example: `PUT /123/mutate/add/12` will add `12` to the number with the id `123`
  * Valid actions: `add`, `subtract`, `multiply`, `divide`.
* `GET /operation/:operationId` - Fetch the current status of the requested operation
  * Returns: Status information about the request operation.
  * Example: `GET /operation/e8bd5ca1-f2ac-4e65-bc2d-a62eca34e992` will return information about
  the requested mutation operation (whether or not it has been applied to its number.)

## Using this repo

This is an [AWS SAM project](https://github.com/awslabs/aws-sam-cli), so the heart of everything is 
`template.yaml`, which describes resources and their configuration.

This project uses API Gateway, Lambda, and DynamoDB. For local debugging, you can use the AWS SAM CLI to run
API Gateway and Lambda. Provided debugging scripts can configure and run [dynamodb-local](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html)
for you.

### Dependencies

* [AWS SAM CLI](https://github.com/awslabs/aws-sam-cli#windows-linux-macos-with-pip-recommended) for local debugging and deployment
* [Docker](https://docs.docker.com/install/) for running dynamodb-local and locally debugging the functions

### Running locally

The AWS SAM CLI is great at running things locally for you. And there are a couple of scripts 
included in this repository that can perform different testing and troubleshooting tasks for you.

* `./start-api.sh` - Runs `dynamodb-local` in a docker container, creates tables and streams mimicing
a real environment and uses `aws sam start-api` to load up the API Gateway
* `process-stream.sh` - Fetches records from the `dynamodb-local` stream for mutation events and feeds 
them into the processor function for aggregation, storage, and later retrieval

## Deploying

Set the following environment variables:
* `CUSTOM_DOMAIN_NAME`: The domain on which you'd like to host the deployed app
* `HOSTED_ZONE_ID`: The hosted zone id of the custom domain

The included `./deploy.sh` can take care of deployment for you.

The [`sam package` command](https://github.com/awslabs/aws-sam-cli#package-and-deploy-to-lambda) needs an S3 bucket
to publish to, so you'll need to create that beforehand. You can set it using the environment variable `SAM_DEPLOYMENT_BUCKET`
or by editing the script to hardcode your intended value.

## To Do
* [ ] Make `./process-stream.sh` better. Always retrieves all records from the first shard. What about other shards and replay events?
* [ ] Consider some idempotency protection. Not really something to worry too much about in production due to the guarantees of dynamo streams, but helpful for demonstration purposes and local troubleshooting.
* [ ] Local tests. Local debugging is nice, but having instrumented tests is even easier.
* [ ] Document a few extensibility thought experiments. Part of what I'm trying to demonstrate here is how effortlessly extensible this
pattern is. Document a few examples (webhooks on number updates, replaying events to re-build aggregates, etc).