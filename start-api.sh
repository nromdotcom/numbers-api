# Kill running dynamo containers, if any
docker kill dynamodb || true
docker rm dynamodb || true
docker network rm labmda_local || true

# Create lambda_local network
docker network create lambda_local

# Run and bootstrap dynamo-local
docker run --rm --network lambda_local -P --name dynamodb -d amazon/dynamodb-local:latest \
  -jar DynamoDBLocal.jar -sharedDb

docker run -e AWS_ACCESS_KEY_ID=fake -e AWS_SECRET_ACCESS_KEY=fake --rm --network lambda_local \
  lambci/lambda:build-nodejs8.10 aws dynamodb create-table --table-name OperationActionsLocal \
  --attribute-definitions AttributeName=numberId,AttributeType=S AttributeName=operationId,AttributeType=S \
  --key-schema AttributeName=numberId,KeyType=HASH AttributeName=operationId,KeyType=RANGE \
  --global-secondary-indexes "IndexName=operationIndex,KeySchema=[{AttributeName=operationId,KeyType=HASH}],Projection={ProjectionType=ALL},ProvisionedThroughput={ReadCapacityUnits=5,WriteCapacityUnits=5}" \
  --endpoint http://dynamodb:8000 \
  --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
  --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES \
  --region none

docker run -e AWS_ACCESS_KEY_ID=fake -e AWS_SECRET_ACCESS_KEY=fake --rm --network lambda_local \
  lambci/lambda:build-nodejs8.10 aws dynamodb create-table --table-name NumberModelsLocal \
  --attribute-definitions AttributeName=numberId,AttributeType=S \
  --key-schema AttributeName=numberId,KeyType=HASH \
  --endpoint http://dynamodb:8000 \
  --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
  --region none

echo ""
echo "DynamoDB Webshell: http://$(docker port dynamodb 8000/tcp)/shell"
echo ""

# Start our API
sam local start-api --docker-network lambda_local
