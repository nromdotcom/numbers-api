// We can't use the XRay SDK until it supports async/await ಠ_ಠ
// https://github.com/aws/aws-xray-sdk-node/issues/27
const { DynamoDB, Endpoint } = require('aws-sdk')
const VALID_ACTIONS = ['add', 'subtract', 'multiply', 'divide']
let dynamo = new DynamoDB.DocumentClient()

// If we're running locally, talk to DynamoDB-local
if (process.env.AWS_SAM_LOCAL) {
  dynamo = new DynamoDB.DocumentClient({ endpoint: new Endpoint('http://dynamodb:8000') })
  process.env.OPERATION_TABLE_NAME = 'OperationActionsLocal'
  process.env.MODEL_TABLE_NAME = 'NumberModelsLocal'
}

class DataAccess {
  constructor (ddbClient = dynamo) {
    this.dynamoClient = ddbClient
  }

  async putOperation (operationId, numberId, action, amount, status = 'new') {
    if (!VALID_ACTIONS.includes(action)) {
      throw new Error(`Invalid action: ${action}`)
    }

    (await this.dynamoClient.put({
      TableName: process.env.OPERATION_TABLE_NAME,
      Item: {
        operationId,
        numberId,
        action,
        amount,
        status,
        originRegion: process.env.AWS_REGION || 'local'
      }
    }).promise())

    return operationId
  }

  async fetchOperation (operationId) {
    return (await this.dynamoClient.query({
      TableName: process.env.OPERATION_TABLE_NAME,
      IndexName: 'operationIndex',
      KeyConditionExpression: 'operationId = :opId',
      ExpressionAttributeValues: {
        ':opId': operationId
      }
    }).promise()).Items[0]
  }

  async fetchNumber (numberId) {
    let number = (await this.dynamoClient.get({
      TableName: process.env.MODEL_TABLE_NAME,
      Key: {numberId}
    }).promise()).Item

    if (!number) {
      number = {
        numberId,
        value: 0
      }
    }

    return number
  }

  async updateNumber (item) {
    return this.dynamoClient.put({
      TableName: process.env.MODEL_TABLE_NAME,
      Item: item
    }).promise()
  }
}

module.exports = DataAccess
