'use strict'
const DynamoClient = require('./DynamoClient')
const dynamo = new DynamoClient()
const { ok, error, getLogger } = require('./ApiUtils')
const logger = getLogger()

exports.handler = async function (event, context) {
  try {
    if (!event.pathParameters || !event.pathParameters.hasOwnProperty('numberId')) {
      throw new Error('Malformed event: no numberId specified')
    }
    const { numberId } = event.pathParameters
    logger(`Trying to fetch number ${numberId}`)

    return ok(await dynamo.fetchNumber(numberId))
  } catch (err) {
    logger(`Unable to query for number: ${err.message}`)
    logger(err)

    return error({
      message: err.message
    })
  }
}
