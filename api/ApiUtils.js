'use strict'
const debug = require('debug')

function respond (status, body) {
  return {
    statusCode: status,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type, Authorziation',
      'Access-Control-Allow-Methods': 'PUT, GET',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }
}

module.exports = {
  ok (body) {
    return respond(200, body)
  },
  error (body) {
    return respond(500, body)
  },
  getLogger () {
    const parentPath = module.parent.id
    const parentFile = parentPath.split('/')[parentPath.split('/').length - 1]
    const parentModule = parentFile.split('.', 2)[0]

    return debug(`NumbersApi:${parentModule}`)
  }
}
