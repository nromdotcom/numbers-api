'use strict'
const { ok, getLogger } = require('./ApiUtils')
const logger = getLogger()
const pkg = require('./package.json')

exports.handler = async function (event, context) {
  logger('Healthcheck triggered!')

  return ok({
    region: process.env.AWS_REGION,
    version: pkg.version
  })
}
