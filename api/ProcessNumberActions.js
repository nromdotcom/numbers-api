'use strict'
const DynamoClient = require('./DynamoClient')
const dynamo = new DynamoClient()
const { getLogger } = require('./ApiUtils')
const logger = getLogger()

// Apply the operation to the model
function processOperation (number, action, amount, operationId) {
  logger(`Applying ${operationId}: ${number.value} ${action} ${amount}`)
  switch (action) {
    case 'add':
      number.value = (parseInt(number.value) + parseInt(amount)).toString()
      break
    case 'subtract':
      number.value = (parseInt(number.value) - parseInt(amount)).toString()
      break
    case 'multiply':
      number.value = (parseInt(number.value) * parseInt(amount)).toString()
      break
    case 'divide':
      number.value = (parseInt(number.value) / parseInt(amount)).toString()
      break
  }

  number.lastOperation = operationId
  return number
}

function parseRecord (record) {
  return {
    action: record.dynamodb.NewImage.action.S,
    amount: record.dynamodb.NewImage.amount.S,
    numberId: record.dynamodb.NewImage.numberId.S,
    operationId: record.dynamodb.NewImage.operationId.S,
    originRegion: record.dynamodb.NewImage.originRegion.S
  }
}

exports.handler = async function (event, context) {
  const numbers = {}
  logger(`Received a batch of ${event.Records.length} updates...`)

  // Process each record in our batch
  for (let record of event.Records) {
    if (record.eventName !== 'INSERT') {
      logger('Record is not an INSERT, skipping...')
      continue
    }

    let { action, amount, numberId, operationId, originRegion } = parseRecord(record)
    if (!process.env.AWS_SAM_LOCAL && process.env.AWS_REGION !== originRegion) {
      console.log(`Only processing updates for ${process.env.AWS_REGION}, skipping...`)
      return
    }

    logger(`Processing: ${operationId}, ${numberId}, ${action}, ${amount}`)

    if (!numbers.hasOwnProperty(numberId)) {
      logger(`Fetching new number id ${numberId}`)
      numbers[numberId] = await dynamo.fetchNumber(numberId)
    }

    numbers[numberId] = processOperation(numbers[numberId], action, amount, operationId)
    await dynamo.putOperation(operationId, numberId, action, amount, 'done')
  }

  for (let numberId in numbers) {
    logger(`Writing new value ${numbers[numberId].value} for ${numberId}.`)
    await dynamo.updateNumber(numbers[numberId])
  }

  return numbers
}
