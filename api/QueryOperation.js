'use strict'
const DynamoClient = require('./DynamoClient')
const dynamo = new DynamoClient()
const { ok, error, getLogger } = require('./ApiUtils')
const logger = getLogger()

exports.handler = async function (event, context) {
  try {
    const { operationId } = event.pathParameters
    logger(`Querying status of ${operationId}`)

    if (!operationId || operationId.length === 0) {
      throw new Error('Specify an operationId!')
    }

    const operation = await dynamo.fetchOperation(operationId)
    if (!operation) {
      throw new Error(`Could not find operation ${operationId}`)
    }

    return ok(operation)
  } catch (err) {
    logger(`Unable to fetch operation status: ${err.message}`)
    logger(err)

    return error({
      message: err.message
    })
  }
}
