'use strict'
const DynamoClient = require('./DynamoClient')
const dynamo = new DynamoClient()
const { ok, error, getLogger } = require('./ApiUtils')
const logger = getLogger()

function actionQueryUrl (host, operationId) {
  return `${process.env.AWS_SAM_LOCAL ? 'http' : 'https'}://${host}/operation/${operationId}`
}

exports.handler = async function (event, context) {
  try {
    const { numberId, action, amount } = event.pathParameters
    let operationId = context.awsRequestId
    logger(`${operationId}, ${numberId}, ${action}, ${amount}`)

    // Write out the operation to the dynamodb table
    operationId = await dynamo.putOperation(operationId, numberId, action, amount)
    logger(`Wrote operation ${operationId}`)

    return ok({
      operationId,
      links: {
        query: actionQueryUrl(event.headers.Host, operationId)
      }
    })
  } catch (err) {
    logger(`Something went wrong: ${err.message}`)
    logger(err)

    // Something has gone wrong. Just always return a 500.
    return error({
      message: err.message
    })
  }
}
