'use strict'
const { getLogger } = require('./ApiUtils')
const logger = getLogger()

const fs = require('fs')
const util = require('util')
const readFile = util.promisify(fs.readFile)

let swagger
async function getSwagger () {
  if (!swagger) {
    swagger = await readFile('./swagger.yaml', 'utf8')
  }

  return swagger
}

exports.handler = async function (event, context) {
  logger('Swagger!')

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type, Authorziation',
      'Access-Control-Allow-Methods': 'PUT, GET',
      'Content-Type': 'application/yaml'
    },
    body: await getSwagger()
  }
}
