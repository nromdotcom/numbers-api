set -e

SAM_DEPLOYMENT_BUCKET='numapi-deployment-artifacts'
STACK_NAME='numbers-api'
ENVIRONMENT='dev'

function ensureNoBucket()
{
  echo "Checking for deployment bucket $1..."
  local bucketCheck=`aws s3api list-buckets --query "contains(Buckets[].[Name][], '$1')"`  
  if [ $bucketCheck = "false" ] ; then
    echo " -> Bucket not found, good..."
  else
    echo " -> Bucket exists, removing..."
    aws s3 rb s3://$1 --force
  fi
}

function undeployRegion()
{
  aws cloudformation delete-stack \
    --stack-name "$STACK_NAME-$ENVIRONMENT" \
    --region $1

  aws cloudformation wait stack-delete-complete \
    --stack-name "$STACK_NAME-$ENVIRONMENT" \
    --region $1
}

function removeGlobalTable()
{
  hasGlobalTable=`aws dynamodb list-global-tables --region us-east-1 \
    --query "contains(GlobalTables[*].[GlobalTableName][],'$1')"`

  if [ $hasGlobalTable = "true" ] ; then
    echo " -> Aggregates Global Table exists, removing..."
    aws dynamodb update-global-table --global-table-name $1 \
      --replica-updates Delete={RegionName=us-east-1},Delete={RegionName=eu-west-1},Delete={RegionName=ap-southeast-1} \
      --region us-east-1
    echo " -> Success!"
  else
    echo " -> Aggregates Global table already exists, continuing..."
  fi
}

# Ensure the DynamoDb Global Table is destroyed
for globalTable in "NumbersApiEvents-$ENVIRONMENT" "NumbersApiAggregates-$ENVIRONMENT"; do
  removeGlobalTable $globalTable
done

for region in 'us-east-1' 'eu-west-1' 'ap-southeast-1'; do
  undeployRegion $region
  ensureNoBucket "$SAM_DEPLOYMENT_BUCKET-$region" $region  
done